<?php

namespace Drupal\wwu_ldap_cas_authmap\EventSubscriber;

use Drupal\Core\Logger\LoggerChannelFactoryInterface;

use Drupal\externalauth\Event\ExternalAuthEvents;
use Drupal\externalauth\Event\ExternalAuthRegisterEvent;
use Drupal\ldap_user\Processor\DrupalUserProcessor;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;

final class WwuLdapCasAuthmapEventSubscriber implements EventSubscriberInterface {

  private $logger;

  private $drupalUserProcessor;

  public static function getSubscribedEvents() {
    return [
      ExternalAuthEvents::REGISTER => 'register',
    ];
  }

  public function __construct(LoggerChannelFactoryInterface $logger_factory, DrupalUserProcessor $drupal_user_processor) {
    $this->logger = $logger_factory->get('wwu_ldap_cas_authmap');
    $this->drupalUserProcessor = $drupal_user_processor;
  }

  public function register(ExternalAuthRegisterEvent $event) {
    $account = $event->getAccount();

    $this->drupalUserProcessor->drupalUserUpdate($account);
    $account->save();
    $this->logger->notice('Provisioned account %username from LDAP.', ['%username' => $event->getAuthname()]);
  }

}
