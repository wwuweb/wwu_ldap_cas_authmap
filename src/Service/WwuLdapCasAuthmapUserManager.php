<?php

namespace Drupal\wwu_ldap_cas_authmap\Service;

use Drupal\ldap_servers\LdapUserManager;
use Drupal\user\UserInterface;

class WwuLdapCasAuthmapUserManager extends LdapUserManager {

  public function getUserDataByAccount(UserInterface $account) {
    $identifier = $this->externalAuth->get($account->id(), 'cas');

    if ($identifier) {
      return $this->getUserDataByIdentifier($identifier);
    }

    return parent::getUserDataByAccount($account);
  }

}
